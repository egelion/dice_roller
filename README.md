Simple D&D Dice Roller

- - Running the Program - -

To run from a unix terminal type the commands as follows:

unix> make
unix> ./dice

The syntax for each roll is as follows:

xdy+z

x = The number of dice to roll

y = dice type

z = modifier

Example: rolling a 3d4 dice with a +4 modifier

unix> ./dice

What type of die will you be rolling? [Format = xdy+z] > 3d4+4

How many rolls? > 3

The outcomes will be displayed here

- - Quitting the program - -

To quit D&D dice roller type quit, exit, e, or q when the
app is asking for what type of die will be rolled for example:

What type of die will you be rolling? [Format = xdy+z] > quit

Thank you, and Enjoy!!

Copyright Evan Boren 11/10/2017
