/* GeLioN's D&D Dice Roller */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <string>
#include <limits>

int main(){
    srand(time(NULL));
    std::string dieType; 
    std::string delimiter = "d";
    std::string delim2 = "+";
    std::string fin = "q";
    std::string fin2 = "quit";
    std::string fin3 = "exit";
    std::string fin4 = "e";
    bool done = false;
    size_t pos = 0; 
    size_t pos2 = 0; 
    std::string val;
    while(!done){
        int numDice = 1;
        int dieVal = 2;
        int numRolls = 1;
        int wid = 1;
        int modifier = 0;
        std::cout << "\nWhat type of die will you be rolling? [Format = xdy+z] > ";
        std::cin >> dieType;
        if(dieType == fin || dieType == fin2 || dieType == fin2 || dieType == fin4){ done = true; break; }
        while((pos = dieType.find(delimiter)) != std::string::npos){
           val = dieType.substr(0, pos); 
           numDice = atoi(val.c_str());
           dieType.erase(0, pos + delimiter.length());
        }
        while((pos2 = dieType.find(delim2)) != std::string::npos){
           val = dieType.substr(0, pos2);
           dieVal = atoi(val.c_str());
           dieType.erase(0, pos2 + delim2.length());
           val = dieType.substr(0, std::string::npos);
           modifier = atoi(val.c_str());
        }
        if(numDice < 1 || numDice > 100 || dieVal < 2 || dieVal > 100){
            std::cout << "Testing: numDice = " << numDice << " dieVal = " << dieVal << " and modifier = " << modifier << std::endl;
            std::cout << "The valid die types are from 1d to 100d + 2 to 100\n";
            std::cin >> dieType;
        }
        //std::cout << "Testing: numDice = " << numDice << " dieVal = " << dieVal << " and modifier = " << modifier << std::endl;
        std::cout << "\nHow many rolls? > ";
        std::cin >> numRolls;
        if(!std::cin){
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        if(dieVal > 9){ wid = 2; }
        if(dieVal == 100){ wid = 3; }
        std::cout << std::endl;
        for(int i=0; i<numRolls; i++){
           int total = 0;
           std::cout << "{";
            for(int j=0; j<numDice; j++){
               int roll = (rand() % dieVal) + 1; 
               std::cout << std::setw(wid) << roll;
               if(j < numDice - 1){ std::cout << ","; }
               total += roll;
            }
            if(modifier > 0){
                total += modifier;
                std::cout << "} + " << modifier << " = " << total << std::endl;
            }
            else{
                std::cout << "} = " << total << std::endl;
            }
        }
    }
    return 0;
}
